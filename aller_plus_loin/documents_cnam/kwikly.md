# KWIKLY 
<!-- SPDX-License-Identifier: MPL-2.0 -->

Katalogue des données SNIIRAM-SNDS

::: tip
[Télécharger le Kwikly](../../files/Cnam/202108_Cnam_KWIKLY-KatalogueSniiramSNDS2_MPL-2.0.xlsm) [CNAM - Version 3.1 - 2021-08-19 - MPL-2.0]. 
:::

Le **Kwikly** est un fichier Excel sur plusieurs onglets décrivant les variables des produits individuels bénéficiaires (type, taille, libellé) :
- DCIR
- PMSI
- DCIRS
- Cartographie
- Causes de décès
- Référentiel Bénéficiaires
- Référentiel Médicalisé

:::tip Historique des mises à jour
- 01.06.2021 : Une nouvelle version (3.1) a été mise en ligne avec notamment la table des bénéficiaires en Etablissements Médico-Sociaux IR_ESM_R et les nouvelles variables dans les tables de DCIR et des référentiels associés, dont l’identifiant cartographie.
- 19.08.2021 : Mise à jour de la version 3.1 avec l’ajout des différentes tables du PMSI 2020 et de leurs variables respectives.
:::

Le Kwikly permet de connaitre la présence des variables dans l’historique des produits depuis 2006.

Le Kwikly est un outil de clics rapides qui attend vos suggestions d’amélioration sur la boite <snds.cnam@assurance-maladie.fr>



